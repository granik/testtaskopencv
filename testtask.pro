include(./qtopencv/opencv.pri)
include(./shared/shared.pri)

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets
    CONFIG += C++17
} else {
    QMAKE_CXXFLAGS += -std=c++0x
}

TARGET = testtask
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    recentfiles.cpp \
    convert.cpp \
    dithering.cpp \
    bloom.cpp

HEADERS  += mainwindow.h \
    recentfiles.h \
    convert.h \
    dithering.h \
    bloom.h

FORMS    += mainwindow.ui

CONFIG += file_copies
COPIES += testimages
testimages.files = $$files(img/*)
testimages.path = $$OUT_PWD/img

RESOURCES += \
    testtask.qrc
