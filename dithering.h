#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace std;
using namespace cv;

uint8_t saturated_add(uint8_t val1, int8_t val2);
Mat quantitize(const Mat &m, int algo);
Mat quantitize_by_stucki(const Mat &m);
Mat quantitize_by_fsteinberg(const Mat &m);
Mat quantitize_by_burkes(const Mat &m);
Mat quantitize_by_sierra(const Mat &m);
Mat quantitize_by_tworowsierra(const Mat &m);
Mat quantitize_by_sierralite(const Mat &m);
Mat greyfilter(const Mat &m, int algo);
Mat rgbfilter(const Mat &m, int channel, int algo);
