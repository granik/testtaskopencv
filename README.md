# README #

### What is this repository for? ###

Test app for image processing

### How do I get set up? ###

Summary of set up

```bash
git clone git@bitbucket.org:granik/testtaskopencv.git
mkdir testtaskopencv-build
cd testtaskopencv-build
qmake ../testtaskopencv
make
```

Dependencies

    * OpenCV ver > 4.2.x

    * Qt ver >  5.9.x

How to run
```bash
./testtask
```
Note

    * Test images are placed in /img folder in build destination directory.
    * Qt-app design was inspired by Debao Zhang (https://github.com/dbzhang800/QtOpenCV) 

Some tricks

    * For begining Open an image then choose a filter and edit parameters.
    * Use Preview button to see a result of the filter action.
    * Click Apply button to apply the filter to the original image, after that you will able to choose next filter.
    * To edit Threshold parameter for blooming you can use the  color values displayed in Status bar.

### Who do I talk to? ###

    * Alex Alexeev

    * granik@ya.ru
