#ifndef ABSTRACTCONVERT_H
#define ABSTRACTCONVERT_H

#include <QPointer>
#include <QString>

namespace cv {
class Mat;
}
class QWidget;
class QSpinBox;
class QComboBox;
class QCheckBox;
class QPlainTextEdit;

class AbstractConvert
{
public:
    AbstractConvert();
    virtual ~AbstractConvert();

    virtual bool applyTo(const cv::Mat &input, cv::Mat &output) = 0;
    QWidget *paramsWidget();
    QString errorString() const;

protected:
    virtual void initParamsWidget() = 0;

    QPointer<QWidget> m_widget;
    QString m_errorString;
};

class Gray : public AbstractConvert
{
public:
    Gray(){}
    ~Gray(){}

    bool applyTo(const cv::Mat &input, cv::Mat &output);

private:
    void initParamsWidget(){}
};

class Dithering : public AbstractConvert
{
public:
    Dithering() {}
    ~Dithering() {}

    bool applyTo(const cv::Mat &input, cv::Mat &output);

protected:
    void initParamsWidget();

    QCheckBox *toGrey;
    QComboBox *rgbEdit;
    QComboBox *algoEdit;
};

class Bloom : public AbstractConvert
{
public:
    Bloom() {}
    ~Bloom() {}

    bool applyTo(const cv::Mat &input, cv::Mat &output);

protected:
    void initParamsWidget();

    QSpinBox *tresEdit;
    QSpinBox *radiusEdit;
    QSpinBox *intensity;
};

#endif // ABSTRACTCONVERT_H
