#include "convert.h"
#include "dithering.h"
#include "bloom.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <QFormLayout>
#include <QComboBox>
#include <QSpinBox>
#include <QCheckBox>
#include <QPlainTextEdit>

static QSpinBox *createSpinBox(int v, int min=0, int max=99)
{
    QSpinBox *edit = new QSpinBox;
    edit->setRange(min, max);
    edit->setValue(v);
    return edit;
}

AbstractConvert::AbstractConvert()
{

}

AbstractConvert::~AbstractConvert()
{
    if (!m_widget.isNull())
        delete m_widget.data();
}

QWidget *AbstractConvert::paramsWidget()
{
    if (m_widget.isNull()) {
        m_widget = new QWidget();
        initParamsWidget();
    }
    return m_widget.data();
}

QString AbstractConvert::errorString() const
{
    return m_errorString;
}


bool Gray::applyTo(const cv::Mat &input, cv::Mat &output)
{
    if (input.channels() == 1) {
        m_errorString = "not a rgb color image.";
        return false;
    }
    cv::cvtColor(input, output, cv::COLOR_RGB2GRAY);
    return true;
}


bool Dithering::applyTo(const cv::Mat &input, cv::Mat &output){

    int algoindex = algoEdit->itemData(algoEdit->currentIndex()).toInt();
    int rgbindex = rgbEdit->itemData(rgbEdit->currentIndex()).toInt();
    if (toGrey->isChecked()) {
        output = greyfilter(input,algoindex);
    } else {
        if (input.channels() == 1) {
            m_errorString = "not a rgb color image.";
            return false;
        }else{
            output = rgbfilter(input,rgbindex,algoindex);
        }
    }

    return true;
}

void Dithering::initParamsWidget(){

    toGrey = new QCheckBox;

    rgbEdit = new QComboBox;
    rgbEdit->addItem("R", 0);
    rgbEdit->addItem("G", 1);
    rgbEdit->addItem("B", 2);
    rgbEdit->addItem("R+G+B", 3);

    algoEdit = new QComboBox;
    algoEdit->addItem("Fon-Steinberg", 0);
    algoEdit->addItem("Stuki", 1);
    algoEdit->addItem("Burkes", 2);
    algoEdit->addItem("Sierra", 3);
    algoEdit->addItem("TwoRowSierra", 4);
    algoEdit->addItem("SierraLite", 5);

    QFormLayout *layout = new QFormLayout(m_widget.data());
    layout->addRow("To Grey", toGrey);
    layout->addRow("Channel", rgbEdit);
    layout->addRow("Algo", algoEdit);

}

bool Bloom::applyTo(const cv::Mat &input, cv::Mat &output){

    int threshold = tresEdit->value();
    int radius = radiusEdit->value();
    double intens = intensity->value();

    if (input.channels() == 1) {
         m_errorString = "not a rgb color image.";
         return false;
    }else{
         output = bloomfilter(input,threshold,radius,intens);
    }

    return true;
}

void Bloom::initParamsWidget(){

    tresEdit = createSpinBox(56, 0, 255);
    radiusEdit = createSpinBox(30, 0, 50);
    intensity = createSpinBox(100, 0, 100);

    QFormLayout *layout = new QFormLayout(m_widget.data());
    layout->addRow("Threshold (0-255)", tresEdit);
    layout->addRow("Radius (0-50)", radiusEdit);
    layout->addRow("Intensity,%", intensity);

}
