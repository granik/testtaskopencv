#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QLatin1String>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("OpenCV_TestTask");
    a.setOrganizationName("Alex Alexeev");
    MainWindow w;

    QFile file(":/Combinear.qss");
    file.open(QFile::ReadOnly);

    QString styleSheet { QLatin1String(file.readAll()) };

    //setup stylesheet
    a.setStyleSheet(styleSheet);

    w.showMaximized();

    return a.exec();
}
