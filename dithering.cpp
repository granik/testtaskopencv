#include "dithering.h"

uint8_t saturated_add(uint8_t val1, int8_t val2)
{
  int16_t val1_int = val1;
  int16_t val2_int = val2;
  int16_t tmp = val1_int + val2_int;

  if(tmp > 255)
  {
    return 255;
  }
  else if(tmp < 0)
  {
    return 0;
  }
  else
  {
    return tmp;
  }
}

Mat quantitize(const Mat &m, int algo){
    switch (algo){
        case 0:
            return quantitize_by_fsteinberg(m);
            break;
        case 1:
            return quantitize_by_stucki(m);
            break;
        case 2:
            return quantitize_by_burkes(m);
            break;
        case 3:
            return quantitize_by_sierra(m);
            break;
        case 4:
            return quantitize_by_tworowsierra(m);
            break;
        case 5:
            return quantitize_by_sierralite(m);
            break;
        default:
            return quantitize_by_fsteinberg(m);
            break;
    }
}

//Floyd-Steinberg algo realization
Mat quantitize_by_fsteinberg(const Mat &m){

    Mat im = m.clone();
    int imgWidth = im.cols;
    int imgHeight = im.rows;
    int err;
    int8_t a,b,c,d;

    for(int i=0;i<imgHeight;i++){
        for(int j=0;j<imgWidth;j++){
            if(im.at<uint8_t>(i,j) > 127)
            {
              err = im.at<uint8_t>(i,j) - 255;
              im.at<uint8_t>(i,j) = 255;
            }
            else
            {
              err = im.at<uint8_t>(i,j) - 0;
              im.at<uint8_t>(i,j) = 0;
            }

            a = (err * 7) / 16;
            b = (err * 1) / 16;
            c = (err * 5) / 16;
            d = (err * 3) / 16;

            if((i != (imgHeight-1)) && (j != 0) && (j != (imgWidth - 1)))
            {
              im.at<uint8_t>(i+0,j+1) = saturated_add(im.at<uint8_t>(i+0,j+1),a);
              im.at<uint8_t>(i+1,j+1) = saturated_add(im.at<uint8_t>(i+1,j+1),b);
              im.at<uint8_t>(i+1,j+0) = saturated_add(im.at<uint8_t>(i+1,j+0),c);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j-1),d);
            }
        }
    }

    return im;
}

//Stuki algo realization
Mat quantitize_by_stucki(const Mat &m){

    Mat im = m.clone();
    int imgWidth = im.cols;
    int imgHeight = im.rows;
    int err;
    int8_t a,d,e,f;

    for(int i=0;i<imgHeight;i++){
        for(int j=0;j<imgWidth;j++){
            if(im.at<uint8_t>(i,j) > 127)
            {
              err = im.at<uint8_t>(i,j) - 255;
              im.at<uint8_t>(i,j) = 255;
            }
            else
            {
              err = im.at<uint8_t>(i,j) - 0;
              im.at<uint8_t>(i,j) = 0;
            }

            a = (err * 8) / 42;
            //b = (err * 7) / 42;
            //c = (err * 5) / 42;
            d = (err * 4) / 42;
            e = (err * 2) / 42;
            f = (err * 1) / 42;


            if((i < (imgHeight-2)) && (j > 1) && (j < (imgWidth - 2)))
            {
              im.at<uint8_t>(i+0,j+1) = saturated_add(im.at<uint8_t>(i+0,j+1),a);
              im.at<uint8_t>(i+1,j+1) = saturated_add(im.at<uint8_t>(i+0,j+2),d);
              im.at<uint8_t>(i+1,j+0) = saturated_add(im.at<uint8_t>(i+1,j-2),e);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j-1),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j),a);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+1),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+2),e);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j-2),f);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j-1),e);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j+1),e);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j+2),f);
            }
        }
    }

    return im;
}

//Stuki algo realization
Mat quantitize_by_burkes(const Mat &m){

    Mat im = m.clone();
    int imgWidth = im.cols;
    int imgHeight = im.rows;
    int err;
    int8_t a,d,e;

    for(int i=0;i<imgHeight;i++){
        for(int j=0;j<imgWidth;j++){
            if(im.at<uint8_t>(i,j) > 127)
            {
              err = im.at<uint8_t>(i,j) - 255;
              im.at<uint8_t>(i,j) = 255;
            }
            else
            {
              err = im.at<uint8_t>(i,j) - 0;
              im.at<uint8_t>(i,j) = 0;
            }

            a = (err * 8) / 32;
            d = (err * 4) / 32;
            e = (err * 2) / 32;


            if((i < (imgHeight-1)) && (j > 1) && (j < (imgWidth - 2)))
            {
              im.at<uint8_t>(i+0,j+1) = saturated_add(im.at<uint8_t>(i+0,j+1),a);
              im.at<uint8_t>(i+1,j+1) = saturated_add(im.at<uint8_t>(i+0,j+2),d);
              im.at<uint8_t>(i+1,j+0) = saturated_add(im.at<uint8_t>(i+1,j-2),e);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j-1),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j),a);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+1),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+2),e);
            }
        }
    }

    return im;
}

Mat quantitize_by_sierra(const Mat &m){

    Mat im = m.clone();
    int imgWidth = im.cols;
    int imgHeight = im.rows;
    int err;
    int8_t a,b,c,d;

    for(int i=0;i<imgHeight;i++){
        for(int j=0;j<imgWidth;j++){
            if(im.at<uint8_t>(i,j) > 127)
            {
              err = im.at<uint8_t>(i,j) - 255;
              im.at<uint8_t>(i,j) = 255;
            }
            else
            {
              err = im.at<uint8_t>(i,j) - 0;
              im.at<uint8_t>(i,j) = 0;
            }

            a = (err * 5) / 32;
            b = (err * 4) / 32;
            c = (err * 3) / 32;
            d = (err * 2) / 32;



            if((i < (imgHeight-2)) && (j > 1) && (j < (imgWidth - 2)))
            {
              im.at<uint8_t>(i+0,j+1) = saturated_add(im.at<uint8_t>(i+0,j+1),a);
              im.at<uint8_t>(i+1,j+1) = saturated_add(im.at<uint8_t>(i+0,j+2),c);
              im.at<uint8_t>(i+1,j+0) = saturated_add(im.at<uint8_t>(i+1,j-2),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j-1),b);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j),a);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+1),b);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+2),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j-1),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j),c);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+2,j+1),d);
            }
        }
    }

    return im;
}

Mat quantitize_by_tworowsierra(const Mat &m){

    Mat im = m.clone();
    int imgWidth = im.cols;
    int imgHeight = im.rows;
    int err;
    int8_t a,b,c,d;

    for(int i=0;i<imgHeight;i++){
        for(int j=0;j<imgWidth;j++){
            if(im.at<uint8_t>(i,j) > 127)
            {
              err = im.at<uint8_t>(i,j) - 255;
              im.at<uint8_t>(i,j) = 255;
            }
            else
            {
              err = im.at<uint8_t>(i,j) - 0;
              im.at<uint8_t>(i,j) = 0;
            }

            a = (err * 4) / 16;
            b = (err * 3) / 16;
            c = (err * 2) / 16;
            d = (err * 1) / 16;


            if((i < (imgHeight-1)) && (j > 1) && (j < (imgWidth - 2)))
            {
              im.at<uint8_t>(i+0,j+1) = saturated_add(im.at<uint8_t>(i+0,j+1),a);
              im.at<uint8_t>(i+1,j+1) = saturated_add(im.at<uint8_t>(i+0,j+2),b);
              im.at<uint8_t>(i+1,j+0) = saturated_add(im.at<uint8_t>(i+1,j-2),d);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j-1),c);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j),b);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+1),c);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j+2),d);
            }
        }
    }

    return im;
}

Mat quantitize_by_sierralite(const Mat &m){

    Mat im = m.clone();
    int imgWidth = im.cols;
    int imgHeight = im.rows;
    int err;
    int8_t a,b;

    for(int i=0;i<imgHeight;i++){
        for(int j=0;j<imgWidth;j++){
            if(im.at<uint8_t>(i,j) > 127)
            {
              err = im.at<uint8_t>(i,j) - 255;
              im.at<uint8_t>(i,j) = 255;
            }
            else
            {
              err = im.at<uint8_t>(i,j) - 0;
              im.at<uint8_t>(i,j) = 0;
            }

            a = (err * 2) / 4;
            b = (err * 1) / 4;


            if((i != (imgHeight-1)) && (j != 0) && (j != (imgWidth - 1)))
            {
              im.at<uint8_t>(i+0,j+1) = saturated_add(im.at<uint8_t>(i+0,j+1),a);
              im.at<uint8_t>(i+1,j+0) = saturated_add(im.at<uint8_t>(i+1,j+0),b);
              im.at<uint8_t>(i+1,j-1) = saturated_add(im.at<uint8_t>(i+1,j-1),b);
            }
        }
    }

    return im;
}

Mat greyfilter(const Mat &m, int algo){
    Mat greyimg;
    if (m.channels() != 1) {
        cvtColor(m,greyimg, cv::COLOR_BGR2GRAY);
    }else{
        greyimg = m;
    }
    return quantitize(greyimg,algo);
}

Mat rgbfilter(const Mat &m, int channel, int algo){
    Mat rgb[3];
    //Mat img2;
    //cvtColor(img,img2, CV_BGR2RGB);
    split(m,rgb);
    switch (channel) {
        case 0:
            rgb[0] = quantitize(rgb[0],algo);
            break;
        case 1:
            rgb[1] = quantitize(rgb[1],algo);
            break;
        case 2:
            rgb[2] = quantitize(rgb[2],algo);
            break;
        default:
            for (auto &ch : rgb)
                ch = quantitize(ch,algo);
            break;
    }

    Mat output_rgb;
    merge(rgb,3,output_rgb);
    return output_rgb;
}
