#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <vector>
using namespace cv;
using namespace std;

Mat bloomfilter(const Mat &im, int threshold, int radius, double intensity);
