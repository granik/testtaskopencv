#include "bloom.h"

Mat bloomfilter(const Mat &im, int threshold, int radius, double intensity){
     //Step 1: Read the original image
    Mat img = im.clone(); //Read the original image

    //Step 2: Convert to grayscale
    Mat imggrey;                            //No need to initialize
    cvtColor(img, imggrey, COLOR_BGR2GRAY); //Convert to grayscale

    //Step 3: Threshold processing
    Mat tmp = img.clone();                  //Clone a temporary image
    //Cycle the threshold processing, the setting less than the threshold is black
    for (int i = 0; i < tmp.rows; i++) //Line loop
    {
        for (int j = 0; j < tmp.cols; j++) //Column loop
        {
            //The setting less than the threshold is black
            if (imggrey.at<uchar>(i, j) < threshold)
            {
                tmp.at<Vec3b>(i, j)[0] = 0; //blue
                tmp.at<Vec3b>(i, j)[1] = 0; //green
                tmp.at<Vec3b>(i, j)[2] = 0; //red

            } //if
        }     //for j
    }         //for i

    //Step 4: Gaussian homogenization + Bilateral Filter
    Mat blur1;                           //Homogenize
    Size ks(9, 9);                      //size
    GaussianBlur(tmp, blur1, ks, 1);     //Gaussian homogenization

    Mat blur11;
    bilateralFilter(tmp,blur11,radius,255,255,1); // Bilateral blur with radious

    Mat blursum;
    addWeighted(blur11, 1.0, blur1, 1.0,1,blursum); // Sum these masks

    //Step 5: Add image
    unsigned int r1; //Temporary variables
    Vec3b *ptr_s;    //Line pointer of the original image
    Vec3b *ptr_d;    //The line pointer of the target graph
    double a = 1.0;
    double b = intensity/100;

    //Progressive scan processing
    for(int i = 0; i < tmp.rows; i++)
    {
        ptr_s = img.ptr<Vec3b>(i);  //Line pointer of the original image
        ptr_d = blursum.ptr<Vec3b>(i); //The line pointer of the target graph

        //Column by column scan processing
        for (int j = 0; j < tmp.cols; j++)
        {
            for (int k = 0; k < 3; k++) //B, G, R cycle for superposition out-of-bounds judgment
            {
                r1 = a * ptr_s[j][k] + b * ptr_d[j][k]; //Weighted average
                if (r1 > 0xff)
                    r1 = 0xff; //Maximum 255

                ptr_d[j][k] = r1; //Assignment

            } //for k
        }     //for j
    }         //for i


    return blursum;

}
